\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[catalan]{babel}
\usepackage{listings}
\usepackage{amstext}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{color} 
\usepackage{parskip}
\usepackage{enumerate}
\usepackage[nottoc]{tocbibind}
\usepackage[backend=biber, style=numeric, bibencoding=ISO-8859-1]{biblatex}
\addbibresource{biblio.bib}




\title{Antecedents}


\begin{document}

\maketitle
%\tableofcontents
\date{}
%\newpage
\section{Antecedents}



Les notes que tractarem es troben en la traducció que fa Ada d'un article original de Luigi Federico Menabrea, anomenat "Notions sur la machine analytique de M. Charles Babbage" \cite{menabrea1842notions}. Ada, a més de traduir-lo a l'anglès, hi afegeix les seves notes ja esmentades. Per tal de posar-les en context, cal parlar de l'article de Menabrea i endinsar-nos en l'història de Babbage i les seves invencions.

\subsection{Primera màquina diferencial}
\label{differential_machine}
Per conèixer d'on prové la motivació de Babbage a començar a automatitzar càlculs, cal anar una mica més enrere en el temps. Ens situem en el 1614, quan John Napier publica "Mirifici logarithmorum canonis descriptio"\cite{napier1914mirifici}. En aquesta obra descriu els logaritmes per primera vegada i dóna pautes per fer-los servir com a eina de càlcul, simplificant i agilitzant les operacions. Permeten, per exemple, fer el producte i quocient de nombres substancialment grans mitjançant sumes i restes. Tot i això, per operar amb logaritmes s'havia de conèixer els valors que prenien i les seves inverses, de manera que calia usar unes taules amb valors precalculats. Aquest mètode va suposar un gran avenç per a tots aquells que havien de treballar amb xifres grans (astrònoms, enginyers, navegants...) i fins fa relativament poc encara s'usaven.

Babbage, durant una visita a França el 1819, descobreix que el govern fabrica taules de logaritmes a gran escala, degut a la gran quantitat de camps on el seu ús era estès. En tornar a Anglaterra decideix posar-se també, juntament amb el seu amic John Herschell, a produir taules de logaritmes per a la seva Societat Astronòmica. Aquestes taules es calculaven, revisaven i imprimien, però sembla que sovint hi havia petits errors, o bé en moment de calcular-les o bé en imprimir-les. Babbage, que tenia la tasca de revisar-les, va pensar en automatitzar tot el procés per tal d'evitar l'error humà. Aquestes taules eren calculades a mà i, en darrera instància, es calculaven avaluant polinomis mitjançant el mètode de les diferències finites. Així doncs, Babbage va pensar que si aconseguia fer una màquina capaç de realitzar aquestes operacions, hauria eliminat l'error humà i les taules sempre serien correctes. 

Babbage comença a pensar com desenvolupar la seva primera màquina el 1821. L'anomenarà \textit{Difference Engine No.1} i només permetrà tabular polinomis. Cal remarcar que aquesta màquina era una calculadora: només era capaç de fer l'operació aritmètica de la suma, de manera que encara no podem parlar d'ordinador/màquina d'ús general. De la mateixa manera, aquesta no és la primera calculadora de la història: Wilhelm Schickard va dissenyar una màquina de sumar el 1623, la Pascalina que Pascal dissenya el 1542, Liebniz en dissenya la primera que permet multiplicar i dividir el 1673, fins i tot, el 1820 l'"aritmòmetre" de Colmar que es produeix en sèrie... per esmentar-ne algunes.

La primera màquina que va dissenyar constava de 24 eixos i 96 engranatges \footnote{Vegeu apartat \ref{mechanic}}, que posteriorment van ser reduïts a només 3 eixos i 18 engranatges. Cap al 1822, quan ja tenia la idea desenvolupada, la va presentar davant la Societat Astronòmica, però no va ser fins el 1824 que el govern Anglès, interessat per les possibilitats de la màquina, finança parcialment el projecte. Babbage comença a fer els plànols detallats de cada una de les peces (no sense abans inventar-se una notació per a descriure processos mecànics i relacions entre diverses parts \footnote{Vegeu Laws of Mechanical Notation \cite{babbage1851laws}}) i contracta a l'enginyer Joseph Climent per dur a terme la fabricació de les peces. Malgrat tot això, no va ser fins el 1830 que Babbage va acabar els plànols de totes les peces, i el 1832 acaben d'unir totes les peces produïdes fins al moment. La part destinada als càlculs era ja funcional, però la idea de Babbage no era només limitar-se a realitzar els càlculs, sinó també a ser capaç d'imprimir-los, cosa que, almenys en la primera màquina diferencial, no va aconseguir. A partir d'aquest moment, a causa de disputes entre Babbage i Climent i la conseqüent negativa del govern de continuar finançant el projecte, s'atura la producció de peces. 

Així doncs, Babbage va acabar amb un prototip funcional de la seva màquina, però limitat només a tres eixos i nombres de fins a cinc xifres. Tan sols va servir com a mostra del seu potencial sense arribar mai a calcular les taules de logaritmes a les que inicialment estava destinada.

Aquest prototip és el que veu Ada Lovelace el 1833, i és el que sembla despertar el seu interès vers les matemàtiques.

\subsubsection{Principi de funcionament de la màquina}
La màquina es basava en el principi de diferències finites per a avaluar polinomis. La idea és crear una taula amb el següent patró:
\[
\begin{array}{ccccccc}
x & p(x) & D & D^2 & D^3 & D^4 & \cdots \\ \hline
x_0 & y_0 \\
&     & \Delta y_0 \\
x_1 & y_1 &             & \Delta^2 y_0\\
&     & \Delta y_1  &              & \Delta^3 y_0\\
x_2 & y_2 &             & \Delta^2 y_1 &             & \Delta^4 y_0\\
&     & \Delta y_2  &              & \Delta^3 y_1 & \vdots \\
x_3 & y_3 &             & \Delta^2 y_2 & \vdots \\
&     & \Delta y_3 & \vdots \\
x_4 & y_4 & \vdots \\
\vdots & \vdots
\end{array}
\]

on $\Delta^n y_k = \Delta^{n-1}y_{k+1} - \Delta^{n-1}y_k$ i $\Delta y_k = y_{k+1} - y_k$. 

Per a un polinomi de grau $n$, $D^n$ és constant, de manera que, avaluant el polinomi per a $n+1$ valors, podem calcular-ne la resta. Per exemple, suposem el polinomi $p(x) = x^2 + x +41$, el polinomi que va utilitzar Babbage en les seves demostracions de funcionament de la màquina diferencial. És un polinomi de segon grau i, per tant, és necessari conèixer tres valors del polinomi: 

\[
\begin{array}{cccc}
x & p(x) & D & D^2 \\ \hline
0 & 41 \\
& 	& 2 \\
1 & 43 & & 2\\
&	& 4 \\
2 & 47 & & 2\\\
&	&	\Delta y_2 \\
x_3 & y_3 
\end{array} 
\]
Aleshores, com que $D^2$ és constant, podem calcular altres valors del polinomi seguint el procediment següent:

Sabem que $\Delta^2 y_1 = 2$, de manera que $\Delta^2 y_1 = \Delta y_2 - \Delta y_1 = 2$ i $\Delta y_1 = 4$, per tant,  com que $\Delta y_2 = \Delta^2 y_1 + \Delta y_1 \implies \Delta y_2 = 4 +2 = 6$. Seguint un raonament similar, veurem que  $y_3 = 47 + 6 = 53$, que coincideix amb $p(3)$. Coneixent aquests valors, ara podem calcular $y_4, y_5, y_6, ...$. Fixem-nos que per a calcular els següents valors d'un polinomi de grau $n$ només necessitem $\Delta^n y_0, \Delta^{n-1} y_1, \cdots , y_n$ (evidentment per a obtenir aquests valors cal haver calculat els anteriors, però a efectes de la màquina, amb aquests és suficient).

\subsubsection{Funcionament mecànic de la màquina}
\label{mechanic}

Internament, la màquina diferencial tenia un conjunt d'eixos: un per a l'avaluació del polinomi, un per a la primera diferència, un per a la segona... i així successivament. Com més eixos té la màquina, més gran és el grau dels polinomis que pot tractar. Aquests eixos contenen engranatges qe s'usen per a codificar els nombres. Cada engranatge té 10 posicions predeterminades per tal de representar els nombres del 0 al 9. Cada eix té un engranatge per a les unitats, un per a les desenes, centenes...  És a dir, que cada eix representa un nombre i cada engranatge un dígit d'aquest nombre. Aquí val la pena aturar-se un moment. Acabem de dir que els engranatges representen els nombres del 0 al 9, de manera que la màquina diferencial funciona en base 10 i no pas en la base 2 a la que estem acostumats. Babbage ho escull així perquè és la base en què nosaltres treballem i era la manera més natural de fer-ho. La màquina analítica que construirà posteriorment també funcionarà en aquesta base i la següent iteració de la diferencial, també.

Un cop els engranatges estan posicionats de manera que representen els nombres que volem utilitzar calcular, cal anar sumant els valors des de l'eix de la última diferència fins a arribar a l'eix del valor. L'eix de la última diferència no es mourà, però a cadascun de la resta d'eixos se li sumarà l'anterior. Per sumar, cada engranatge girarà (i farà girar el de la següent columna) tantes posicions com el nombre que tingui assignat. Per exemple: si l'engranatge de les unitats del primer eix està a la posició corresponent al 3 i el seu homòleg al següent eix està a la posició 5, aleshores el primer engranatge girarà 3 posicions, movent el segon engranatge fins la posició 8. Ara bé, suposem que el primer engranatge segueix a la posició 3, però que el segon ara és a la 9. Ambdós rotaran 3 posicions, i el segon engranatge quedarà a la posició 2. En aquest cas, però, s'haurà registrat que s'ha completat una volta (ens n'emportem 1), de manera que l'engranatge de les desenes farà una rotació extra. Aquest procediment es realitza en dos passos: primer tots els engranatges dels dígits fan rotar els de la columna següent i, un cop tots han rotat i s'han registrat els sobreeiximents, sumem els que ens emportem.


%Internament, la màquina tenia un cilindre d'engranatges per a cada diferència, és a dir, que una màquina amb 3 cilindres podia fer fins a la segona diferència, de manera que podia treballar amb polinomis de tercer grau. Polinomis de grau més alt, requereixen més cilindres. Cada un d'aquests cilindres consisteix en un eix amb engranatges, i cada engranatge representa un dígit. Com més engranatges per eix, més grans poden ser els valors que es tracten. Aquests cilindres eren alhora els que emmagatzemaven els nombres i executaven les sumes. El procediment era el següent: primer   
\printbibliography[resetnumbers=true]

\end{document}